# My AoC solutions

Repo containing the problems that i'm able to resolve of [Advent of Code 2022](https://adventofcode.com/2022).

Usage:
```console
$ ./build.sh
$ ./aoc <day between 1 and 25>
```
