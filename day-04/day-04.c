#include <stdio.h>
#include <stdlib.h>

#define D4_BUFFER_SIZE 256

int day04(const char * filename) {
	char buf[D4_BUFFER_SIZE];
	int num1_buf = 0;
	int num2_buf = 0;
	int range[2] = {0};
	int last_symbol_pos = 0;
	int pairs = 0;
	int pairs_p2 = 0;

	FILE * file;
	if ((file = fopen(filename, "r")) == NULL) {
		fprintf(stderr, "Failed to open file %s.\n", filename);
		return AOC_ERROR;
	}

	while (fgets(buf, D4_BUFFER_SIZE, file) != NULL) {
		last_symbol_pos = 0;
		for (int i = 0; buf[i] != '\0'; ++i) {
			switch (buf[i]) {
				case '-':
					buf[i] = '\0';
					num1_buf = atoi(buf + last_symbol_pos + (last_symbol_pos != 0));
					last_symbol_pos = i;
					buf[i] = '-';
					break;
				case ',':
					buf[i] = '\0';
					num2_buf = atoi(buf + last_symbol_pos + 1);
					last_symbol_pos = i;
					buf[i] = ',';

					range[0] = num1_buf;
					range[1] = num2_buf;
					break;
				case '\n':
					buf[i] = '\0';
					num2_buf = atoi(buf + last_symbol_pos + 1);
					last_symbol_pos = i;

					if ((range[0] <= num1_buf && range[1] >= num2_buf) ||
					    (range[0] >= num1_buf && range[1] <= num2_buf)) {
						pairs += 1;
						pairs_p2 += 1;
					} else if ((range[0] <= num1_buf && num1_buf <= range[1]) ||
					           (range[0] <= num2_buf && num2_buf <= range[1])) {
						pairs_p2 += 1;
					}
					break;
				default: break;
			}
		}
	}
	fclose(file);

	printf("Pairs: %d\n", pairs);
	printf("Pairs (part 2): %d\n", pairs_p2);

	return COMPLETED;
}
