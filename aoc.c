#include "aoc.h"
#include "day-01/day-01.c"
#include "day-02/day-02.c"
#include "day-03/day-03.c"
#include "day-04/day-04.c"
#include "day-05/day-05.c"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int run_aoc_solution(size_t day) {
	switch (day) {
		case 1: return day01("day-01/input.txt");
		case 2: return day02("day-02/input.txt");
		case 3: return day03("day-03/input.txt");
		case 4: return day04("day-04/input.txt");
		case 5: return day05();
		default: return NOT_STARTED;
	}
}

int main(int argc, char ** argv) {
	if (argc < 2) {
		printf("No argument given.\n");
		return 1;
	}

	int day = atoi(argv[1]);
	if (day == 0) {
		printf("No valid number given.\n");
		return 1;
	}
	if (day < 0 || day > 25) {
		printf("Number has to be between 1 and 25.\n");
		return 1;
	}
	printf("Day %d of AoC.\n", day);
	switch (run_aoc_solution(day)) {
		case COMPLETED: break;
		case UNFINISHED:
			printf("The solution of the problem has not been finished.\n");
			break;
		case NOT_STARTED:
			printf("The solution has not been created.\n");
			break;
		case AOC_ERROR:
			printf("An error has occured.\n");
			return 1;
		default:
			printf("Return value is not enumerated. Exiting.\n");
			return 1;
	}
}
