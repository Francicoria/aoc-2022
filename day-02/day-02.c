#include <stdio.h>
#include <stdlib.h>

#define D2_BUFFER_SIZE 128

int part2(int opp, int ending) {
	switch (ending) {
		case 1:
			// lost
			// opp = 3 -> me = 2
			// opp = 2 -> me = 1
			// opp = 1 -> me = 3
			return (opp != 1) ? opp - 1 : 3;
		case 2:
			// draw
			return opp + 3;
		case 3:
			// win
			// opp = 3 -> me = 1
			// opp = 2 -> me = 3
			// opp = 1 -> me = 2
			return ((opp != 3) ? opp + 1 : 1) + 6;
		default:
			printf("unreachable\n");
			exit(1);
	}
}

int day02(const char * filename) {
	char buf[D2_BUFFER_SIZE];
	int total_score = 0;
	int total_score_part2 = 0;

	FILE * file;
	if ((file = fopen(filename, "r")) == NULL) {
		fprintf(stderr, "Failed to open file %s.\n", filename);
		return AOC_ERROR;
	}

	while (fgets(buf, D2_BUFFER_SIZE, file) != NULL) {
		int opp = buf[0] - 64;
		int me = buf[2] - 87;
		switch ((opp - me)*((opp - me < 0) ? -1 : 1)) {
			case 0:
				// tie
				total_score += 3;
				break;
			case 1:
				// lost
				total_score += (opp > me) ? 0 : 6;
				break;
			case 2:
				// win
				total_score += (opp < me) ? 0 : 6;
				break;
			default:
				printf("unreachable\n");
				return AOC_ERROR;
		}
		total_score += me;
		total_score_part2 += part2(opp, me);
	}
	fclose(file);

	printf("Total score: %d\n", total_score);
	printf("Total score (part 2): %d\n", total_score_part2);

	return COMPLETED;
}
