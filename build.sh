#!/bin/sh

CC="gcc"
CFLAGS="-std=c99 -O2 -Wall -Wextra -Wpedantic"
SRC="aoc.c"
EXE="aoc"

set -xe

rm -f ${EXE}
exec ${CC} ${CFLAGS} ${SRC} -o ${EXE}
