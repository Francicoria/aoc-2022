#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define D1_BUFFER_SIZE 512
#define D1_TOP_SIZE    3

int day01(const char * filename) {
	char buf[D1_BUFFER_SIZE];
	int top_elfs[D1_TOP_SIZE] = {0};
	int top_sum = 0;
	int current_calories = 0;

	FILE * file;
	if ((file = fopen(filename, "r")) == NULL) {
		fprintf(stderr, "Failed to open file %s.\n", filename);
		return AOC_ERROR;
	}

	while (fgets(buf, D1_BUFFER_SIZE, file) != NULL) {
		if (buf[0] == '\n') {
			for (int i = D1_TOP_SIZE - 1; i >= 0; --i) {
				if (current_calories > top_elfs[i]) {
					if ((i == 0) || current_calories < top_elfs[i - 1]) {
						top_elfs[i] = current_calories;
						break;
					}
					top_elfs[i] = top_elfs[i - 1];
				}
			}
			current_calories = 0;
		} else {
			current_calories += atoi(buf);
		}
	}
	fclose(file);

	for (int i = 0; i < D1_TOP_SIZE; ++i) {
		top_sum += top_elfs[i];
		printf("%d. %d\n", i + 1, top_elfs[i]);
	}
	printf("Calories sum of top: %d\n", top_sum);

	return COMPLETED;
}
