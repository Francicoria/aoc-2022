#include <stdio.h>
#include <string.h>

#define D3_BUFFER_SIZE 512
#define D3_GROUP_SIZE 3

int day03(const char * filename) {
	char buf[D3_BUFFER_SIZE];
	char group_buf[D3_GROUP_SIZE - 1][D3_BUFFER_SIZE];
	int priorities_sum = 0;
	int priorities_sum_p2 = 0;
	int members_count = 0;

	FILE * file;
	if ((file = fopen(filename, "r")) == NULL) {
		fprintf(stderr, "Failed to open file %s.\n", filename);
		return AOC_ERROR;
	}

	while (fgets(buf, D3_BUFFER_SIZE, file) != NULL) {
		for (int i = 0; i < (int) strlen(buf)/2; ++i) {
			if (strchr(buf + (int) strlen(buf)/2, buf[i]) != NULL) {
				priorities_sum += buf[i] - ((buf[i] > 'Z') ? 96 : 38);
				break;
			}
		}

		strcpy(group_buf[members_count++], buf);
		if (members_count > 2) {
			for (int i = 0; group_buf[0][i] != '\n'; ++i) {
				if (strchr(group_buf[1], group_buf[0][i]) != NULL &&
				    strchr(group_buf[2], group_buf[0][i]) != NULL) {
					priorities_sum_p2 += group_buf[0][i] - ((group_buf[0][i] > 'Z') ? 96 : 38);
					break;
				}
			}
			members_count = 0;
		}
	}
	fclose(file);

	printf("Sum of priorities: %d\n", priorities_sum);
	printf("Sum of badges priorities: %d\n", priorities_sum_p2);

	return COMPLETED;
}
